% Implementation of FCC (Face centered cubic packing) method:
% The method creates a mesh and puts the radioactive capsules
% alongside it's grid points
% By opimizing it's parameters the method calculates how well the
% constraints are satisfied and minimizes the overall difference
% @params: cubeParams is an array of 4 argument:
% resolution of the cube; x, y, z shifting
% @return: overall difference from the given constraints
function objFuncVal = fccVD2_disp(cubeParams)

% Get the parameters from the input
step = cubeParams(1);
x = cubeParams(2);
y = cubeParams(3);
z = cubeParams(4);

fprintf('Model parameters:\n');

% Calculate the location of the seeds
% Seeds must be within the prostate but no too close to it's membrane
% And also not in the uretra and not too close to it
[CX, CY, CZ] = fccStruct(step, x, y, z);
safetyBound = 0.5;
[seedsX, seedsY, seedsZ] = insideSphere(CX, CY, CZ, safetyBound);

fprintf('Safety bound: %.3f\n', safetyBound);

seedCount = [seedsX,seedsY,seedsZ];
nOfSeeds = nnz(any(seedCount,2));

figure('Name','Seed points and measurement points','NumberTitle','off');
plot3(seedsX(:), seedsY(:), seedsZ(:), '*y');
hold on;

% Dose measurement default parameters
cubeSize = 30; % Measurement is always done in a 30x30x30 cube
constVal = 7;
safetyBound = 0.0;

% Measure dose values in a new mesh
% Values measured in the whole sphere without the cylinder
[MX, MY, MZ] = createCube(cubeSize, 0, 0, 0);
[MX, MY, MZ] = insideSphere(MX, MY, MZ, safetyBound);
doseSphere = doseMeasurement(MX, MY, MZ, seedsX, seedsY, seedsZ, constVal);
doseSphere(doseSphere < 0) = 0;

plot3(MX(:), MY(:), MZ(:), '.r');
hold on;

% Measure the same dose values for the cylinder
[MIX, MIY, MIZ] = createCube(cubeSize, 0, 0, 0);
[MIX, MIY, MIZ] = insideCylinder(MIX, MIY, MIZ, safetyBound);
doseCylinder = doseMeasurement(MIX, MIY, MIZ, seedsX, seedsY, seedsZ, constVal);
doseCylinder(doseCylinder < 0) = 0;

plot3(MIX(:), MIY(:), MIZ(:), '.g');
hold on;

% Measure the same dose values for the cylinder
[MRX, MRY, MRZ] = createCube(cubeSize, 0, 0, 0);
[MRX, MRY, MRZ] = insideHalfCylinder(MRX, MRY, MRZ, safetyBound);
doseHalfCylinder = doseMeasurement(MRX, MRY, MRZ, seedsX, seedsY, seedsZ, constVal);
doseHalfCylinder(doseHalfCylinder < 0) = 0;

plot3(MRX(:), MRY(:), MRZ(:), '.b');
hold off;

% figure('Name','Measurement points','NumberTitle','off');
% plot3(MX(:), MY(:), MZ(:), '.r');
% hold on;
% plot3(MIX(:), MIY(:), MIZ(:), '.g');
% hold on;
% plot3(MRX(:), MRY(:), MRZ(:), '.b');
% hold off;

% Constraint prep
rectumVol = ((acosd(0.5/2)*2)*2*pi/180)*4.4*0.15;
rectumGrids = 2/rectumVol;

% Constraints
doseSphereCols = doseSphere(:);
doseCylinderCols = doseCylinder(:);
doseHalfCylinderCols = doseHalfCylinder(:);
V100 = sum(((doseSphereCols ./ 145) .* 100) >= 100) / nnz(doseSphereCols) *100; % V100>=95
D90 = (maxk(doseSphereCols, 0.9) / 145 ) * 100; % D90>=110
% V150 = sum(doseSphereCols > 150) / nnz(doseSphereCols) *100; % V150<=60
Du10 = (maxk(doseCylinderCols, 0.1) / 145) * 100; % Du10<=150
Du30 = (maxk(doseCylinderCols, 0.3) / 145) * 100; % Du30<=130
Dr2cm3 = (maxk(doseHalfCylinderCols, rectumGrids) / 145) * 100; % Dr2cm3<=100

% Plot histrograms to see how dose values distribute
sphereHistData = nonzeros(doseSphereCols);
cylinderHistData = nonzeros(doseCylinderCols);
halfCylinderHistData = nonzeros(doseHalfCylinderCols);

sphereLabel = {};
for i = 1:10
    sphereLabel{i} = [num2str((i-1)*100) '-' num2str(i*100)];
end
sphereLabel{end+1} = '1000-';

figure('Name','Histogram of the Prostate dose values','NumberTitle','off');
% histogram(sphereHistData);
sphereHist = histcounts(sphereHistData ,[0:100:1000 max(sphereHistData(:))]);
bar(sphereHist);
set(gca, 'YScale', 'log');
xticklabels(sphereLabel);
title('Prostate dose values distribution - logarithmic scale');
xlabel('Dose values');
xtickangle(45);
ylabel('Quantity');

figure('Name','Histogram of the Uretra dose values','NumberTitle','off');
histogram(cylinderHistData);
title('Uretra dose values distribution');
xlabel('Dose values');
ylabel('Quantity');

figure('Name','Histogram of the Rectum dose values','NumberTitle','off');
histogram(halfCylinderHistData);
title('Rectum dose values distribution');
xlabel('Dose values');
ylabel('Quantity');

% dDVH
[spheredDVHy,spheredDVHx] = histcounts(sphereHistData ,[0:10:700]);
spheredDVHx(1) =[];
[cylinderdDVHy,cylinderdDVHx] = histcounts(cylinderHistData ,[0:10:700]);
cylinderdDVHx(1) =[];
[halfCylinderdDVHy,halfCylinderdDVHx] = histcounts(halfCylinderHistData ,[0:10:700]);
halfCylinderdDVHx(1) =[];
figure('Name','Prostate, urethra and rectum dDVH','NumberTitle','off');
plot(spheredDVHx,spheredDVHy./length(sphereHistData)*100,'r','LineWidth',2.5);
hold on;
plot(cylinderdDVHx,cylinderdDVHy./length(cylinderHistData)*100,'g','LineWidth',2.5);
hold on;
plot(halfCylinderdDVHx,halfCylinderdDVHy./length(halfCylinderHistData)*100,'b','LineWidth',2.5);
title('Prostate, urethra and rectum dDVH');
set(gca,'XLim',[0 700]);
xlabel('Dose values');
ylabel('Volume (%)');
legend('prostate','urehtra','rectum');
hold off;

% cDVH
sphereHistDataSorted = sort(sphereHistData,'ascend');
sphereHistDataSorted=(sphereHistDataSorted(sphereHistDataSorted<=700));
for i=1:length(sphereHistDataSorted)
    spheredDVHData(i) = length(sphereHistDataSorted(i:end))/length(sphereHistDataSorted);
end
zeroHistData = 0:min(sphereHistDataSorted);
zeroDVHData = ones(1,length(zeroHistData));
sphereHistDataSorted = [zeroHistData(:);sphereHistDataSorted(:)];
spheredDVHData = [zeroDVHData(:);spheredDVHData(:)];

cylinderHistDataSorted = sort(cylinderHistData,'ascend');
cylinderHistDataSorted=(cylinderHistDataSorted(cylinderHistDataSorted<=700));
for i=1:length(cylinderHistDataSorted)
    cylinderdDVHData(i) = length(cylinderHistDataSorted(i:end))/length(cylinderHistDataSorted);
end
zeroHistData = 0:min(cylinderHistDataSorted);
zeroDVHData = ones(1,length(zeroHistData));
cylinderHistDataSorted = [zeroHistData(:);cylinderHistDataSorted(:)];
cylinderdDVHData = [zeroDVHData(:);cylinderdDVHData(:)];

halfCylinderHistDataSorted = sort(halfCylinderHistData,'ascend');
halfCylinderHistDataSorted=(halfCylinderHistDataSorted(halfCylinderHistDataSorted<=700));
for i=1:length(halfCylinderHistDataSorted)
    halfCylinderdDVHData(i) = length(halfCylinderHistDataSorted(i:end))/length(halfCylinderHistDataSorted);
end
zeroHistData = 0:min(halfCylinderHistDataSorted);
zeroDVHData = ones(1,length(zeroHistData));
halfCylinderHistDataSorted = [zeroHistData(:);halfCylinderHistDataSorted(:)];
halfCylinderdDVHData = [zeroDVHData(:);halfCylinderdDVHData(:)];

figure('Name','Prostate, urethra and rectum cDVH','NumberTitle','off');
plot(sphereHistDataSorted,spheredDVHData,'r','LineWidth',2.5);
hold on;
plot(cylinderHistDataSorted,cylinderdDVHData,'g','LineWidth',2.5);
hold on;
plot(halfCylinderHistDataSorted,halfCylinderdDVHData,'b','LineWidth',2.5);
title('Prostate, urethra and rectum cDVH');
set(gca,'XLim',[0 700]);
xlabel('Dose values');
ylabel('Volume (%)');
legend('prostate','urehtra', 'rectum');
hold off;

% Difference from given constraints
V100d = 0.0;
D90d = 0.0;
% V150d = 0.0;
Du10d = 0.0;
Du30d = 0.0;
Dr2cm3d = 0.0;
nOfSeedsd = 0.0;

V100String = '++';
D90String = '++';
% V150String = '++';
Du10String = '++';
Du30String = '++';
Dr2cm3String = '++';
nOfSeedsString = '++';

if (V100 < 95)
    V100String = '--';
    V100d = 95 - V100;
end
if (D90 < 110)
    D90String = '--';
    D90d = (110 - D90);
end
% if (V150 > 60)
%     V150String = '--';
%     V150d = V150 - 60;
% end
if (Du10 > 150)
    Du10String = '--';
    Du10d = (Du10 - 150);
end
if (Du30 > 130)
    Du30String = '--';
    Du30d = (Du30 - 130);
end
if (Dr2cm3d > 100)
    Dr2cm3String = '--';
    Dr2cm3d = (Dr2cm3 - 100);
end
if (nOfSeeds > 70)
    nOfSeedsString = '--';
    nOfSeedsd = (nOfSeeds - 70);
end

fprintf('Constraints:\n');
fprintf('- V100: %.2f%% >= 95%% %s (error: %.2f%%)\n', V100, V100String, V100d);
fprintf('-  D90: %.2f%% >= 110%% %s (error: %.2f%%)\n', D90, D90String, D90d);
% fprintf('- V150: %.2f%% <= 60%% %s (error: %.2f%%)\n', V150, V150String, V150d);
fprintf('- Du10: %.2f%% <= 150%% %s (error: %.2f%%)\n', Du10, Du10String, Du10d)
fprintf('- Du30: %.2f%% <= 130%% %s (error: %.2f%%)\n', Du30, Du30String, Du30d);
fprintf('- Dr2cm3: %.2f%% <= 100%% %s (error: %.2f%%)\n', Dr2cm3, Dr2cm3String, Dr2cm3d);
fprintf('- seeds: %d <= 70 %s (error: %d)\n', nOfSeeds, nOfSeedsString, nOfSeedsd);

% Use weights to force the objective function to find the minimum
V100Weight = 4;
D90Weight = 1;
% V150Weight = 1;
Du10Weight = 1;
Du30Weight = 1;
Dr2cm3Weight = 1;
seedWeight = 1;

objFuncVal = V100d * V100Weight +...
    D90d * D90Weight + ...
    Du10d * Du10Weight + ...
    Du30d * Du30Weight + ...
    Dr2cm3d * Dr2cm3Weight + ...
    nOfSeedsd * seedWeight;
fprintf('Number of seeds: %d\n', nOfSeeds);
fprintf('Total difference (obj. func.): %.2f%%\n', objFuncVal);
fprintf('Used weight in objective function: \n');
fprintf('- V100: %.2f\n', V100Weight);
fprintf('-  D90: %.2f\n', D90Weight);
% fprintf('- V150: %.2f\n', V150Weight);
fprintf('- Du10: %.2f\n', Du10Weight)
fprintf('- Du30: %.2f\n', Du30Weight);
fprintf('- Dr2cm3: %.2f\n', Dr2cm3Weight);
fprintf('- seeds: %.2f\n',seedWeight);
end


% Create the FCC stucture to locate seed points
% Step length and shitf values come from the parameters
function [X, Y, Z] = fccStruct(step, x, y, z)
    % Unit cube with the required base points
    fccbase = [dec2bin(0:7) - ...
        '0'; 0.5 0.5 0; 0.5 0.5 1; 0.5 0 0.5; 0.5 1 0.5; 0 0.5 0.5; 1 0.5 0.5];
    fccbase = fccbase * step;
    
    % Create the grid with the reqiured step length
    [GX, GY, GZ] = ndgrid(0:step:15, 0:step:15, 0:step:15);
    fccXYZ = [GX(:), GY(:), GZ(:)];
    fccXYZ = repmat([GX(:), GY(:), GZ(:)], 14, 1);
    
    % Shift small cube, so the whole grid is covered
    fccXYZ = fccXYZ + ...
        reshape(permute(repmat(fccbase, [1, 1, prod(size(GX))]), [3 1 2]), [], 3);
    fccXYZ = unique(fccXYZ, 'rows');
    
    % Shitf to our dimensions        
    fccXYZ(:,1) = fccXYZ(:, 1) - 3 + x;
    fccXYZ(:,2) = fccXYZ(:, 2) - 3 + y;
    fccXYZ(:,3) = fccXYZ(:, 3) - 3 + z;
    
    % Save the result
    X = fccXYZ(:, 1);
    Y = fccXYZ(:, 2);
    Z = fccXYZ(:, 3);
end

% Creates a regular cube with side length of 5 and center in origin
% Values can be bounded if option is set
function [X, Y, Z] = createCube(res, x, y, z)
    x = linspace(-2.2+x,x+2.2,res);
    y = linspace(-3.67+y,y+2.2,res+10);
    z = linspace(-2.2+z,z+2.2,res);

    [X, Y, Z] = meshgrid(x,y,z);
end

% Throw away those points which are outside from the prostate
% or are inside the urethra
% A safetyBound can be set, so seeds cannot be too close to any of the organs
function [X, Y, Z] = insideSphere(X, Y, Z, safetyBound)
    healDist = sqrt(X.^2 + Y.^2 + Z.^2);
    saveDist = sqrt(X.^2 + Y.^2);
    
    sphereRadius = 2.2;
    cylinderRadius = 0.5;
    X(healDist > sphereRadius) = 0;
    Y(healDist > sphereRadius) = 0;
    Z(healDist > sphereRadius) = 0;
    
    X(saveDist < cylinderRadius + safetyBound) = 0;
    Y(saveDist < cylinderRadius + safetyBound) = 0;
    Z(saveDist < cylinderRadius + safetyBound) = 0;
end

% Keep only the points inside the urethra
function [X, Y, Z] = insideCylinder(X, Y, Z, safetyBound)
    saveDist = sqrt(X.^2 + Y.^2);
    
    cylinderRadius = 0.5;
    X(saveDist > cylinderRadius + safetyBound) = 0;
    Y(saveDist > cylinderRadius + safetyBound) = 0;
    Z(saveDist > cylinderRadius + safetyBound) = 0;
end

% Keep only the points inside the rectum
function [X, Y, Z] = insideHalfCylinder(X, Y, Z, safetyBound)
    saveDist = sqrt(X.^2 + (Y+4.32).^2);
    
    outerRadius = 2;
    innerRadius = 1.85;    
    X(saveDist > outerRadius | saveDist < innerRadius) = 0;
    Y(saveDist > outerRadius | saveDist < innerRadius) = 0;
    Z(saveDist > outerRadius | saveDist < innerRadius) = 0;
end

% Measure for each meshgrid point the dose values coming from all the seeds
% Dose value is inversely proportional to distance
function dose = doseMeasurement(X, Y, Z, seedsX, seedsY, seedsZ, constVal)
    dose = zeros(size(X,1), size(X,2), size(X,3));
    indexes = find(seedsX)';
    for dim = indexes
        Xdist = X(:,:,:) - repmat(seedsX(dim), size(X,1), size(X,2), size(X,3));
        Xdist(Xdist == -seedsX(dim)) = 0;
        Ydist = Y(:,:,:) - repmat(seedsY(dim), size(X,1), size(X,2), size(X,3));
        Ydist(Ydist == -seedsY(dim)) = 0;
        Zdist = Z(:,:,:) - repmat(seedsZ(dim), size(X,1), size(X,2), size(X,3));
        Zdist(Zdist == -seedsZ(dim)) = 0;
        dose = dose + ((1./sqrt(Xdist.^2 + Ydist.^2 + Zdist.^2)).^2).*constVal;
    end
    dose(isinf(dose)) = 0;
end

% Self implementation of the Matlab built-in maxk function
function value = maxk(cols, part)
    sorted = sort(cols, 'descend');  % Sort the values in descending order
    % Don't deal with zero values because they are not relevant
    index = ceil(part*nnz(sorted));
    if index == 0
        index = 1; end
    value = sorted(index);
end