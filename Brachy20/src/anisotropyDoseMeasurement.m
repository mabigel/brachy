% Implementation of the AAPM TG-43 2D dose calculation formalism
% @params: object points, seed ponints, prepared radial dose function and
% anistoropy dose function
% @return: calculated cumulative dose matrix for the object
% author: mabigel

function doseMatrix = anisotropyDoseMeasurement(X3, Y3, Z3, seedsX, seedsY, seedsZ, gF, aF)

dim1 = size(X3,1);
dim2 = size(X3,2);
dim3 = size(X3,3);

% Built-in variables and look-up tables for tabular data
Sk = 0.502;
lambda = 0.965;
L = 0.3;

doseMatrix = zeros(dim1, dim2, dim3);
indexes = find(seedsX)';

for dim = indexes
    
    % distances
    distances3 = pdist2([X3(:) Y3(:) Z3(:)], [seedsX(dim),seedsY(dim),seedsZ(dim)]);
    distances3(distances3 == pdist2([0 0 0], [seedsX(dim),seedsY(dim),seedsZ(dim)])) = 0;
    distanceMatrix3 = reshape(distances3, dim1, dim2, dim3);
    % upper end
    distances3u = pdist2([X3(:) Y3(:) Z3(:)], [seedsX(dim),seedsY(dim),seedsZ(dim)+L/2]);
    distances3u(distances3u == pdist2([0 0 0], [seedsX(dim),seedsY(dim),seedsZ(dim)+L/2])) = 0;

    % angles
    angles3cos = (distanceMatrix3(:).^2 + (L/2)^2 - distances3u(:).^2) ./ (2 * distanceMatrix3(:) * (L/2));
    angles3cos = min(max(angles3cos,-1),1);
    angles3 = acosd(angles3cos);
    anglesMatrix3 = reshape(angles3, dim1, dim2, dim3);
    anglesMatrix3 = round(anglesMatrix3, 2);
    anglesMatrix3(anglesMatrix3>90) = 180 - anglesMatrix3(anglesMatrix3>90);
    anglesMatrix3r = deg2rad(anglesMatrix3);

    % Get interpolated data for the radial dose and anisotropy functions
    % from the griddedInterpolant
    gL = gF(distanceMatrix3);
    FrTeta = aF(distanceMatrix3,anglesMatrix3);

    for i=1:size(distanceMatrix3, 1)
        for j=1:size(distanceMatrix3, 2)
            for k=1:size(distanceMatrix3, 3)
                if distanceMatrix3(i,j,k) ~= 0
                    if anglesMatrix3(i,j,k) == 0
                        GLval = 1 / (distanceMatrix3(i,j,k)^2 - (L^2 / 4));
                    else
                        beta = atan((distanceMatrix3(i,j,k) * cos(anglesMatrix3r(i,j,k)) + L/2) / (distanceMatrix3(i,j,k) * sin(anglesMatrix3r(i,j,k)))) - ...
                            atan(((distanceMatrix3(i,j,k) * cos(anglesMatrix3r(i,j,k))) - L/2) / (distanceMatrix3(i,j,k) * sin(anglesMatrix3r(i,j,k))));
                        GLval = beta / (L * distanceMatrix3(i,j,k) * sin(anglesMatrix3r(i,j,k)));
                    end
                    beta0 = atan((1 * cos(1.5708) + L/2) / (1 * sin(1.5708))) - ...
                        atan(((1*cos(1.5708)) - L/2) / (1 * sin(1.5708)));
                    GLval0 = beta0 / (L * 1 * sin(1.5708));
                    doseMatrix(i,j,k) = doseMatrix(i,j,k) + (Sk * lambda * (GLval / GLval0) * gL(i,j,k) * FrTeta(i,j,k));
                end
            end
        end
    end    
end
doseMatrix(doseMatrix < 0) = 0;
% Multiply with the time factor, the half-life of iodine is 59.4 days
doseMatrix = (doseMatrix .* (24 * 59.4 / log(2))) ./ 100;
end