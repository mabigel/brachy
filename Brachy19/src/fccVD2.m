% Implementation of FCC (Face centered cubic packing) method:
% The method creates a mesh and puts the radioactive capsules
% alongside it's grid points
% By opimizing it's parameters the method calculates how well the
% constraints are satisfied and minimizes the overall difference
% @params: cubeParams is an array of 4 argument:
% resolution of the cube; x, y, z shifting
% @return: overall difference from the given constraints
function objFuncVal = fccVD2(cubeParams)

% Get the parameters from the input
step = cubeParams(1);
x = cubeParams(2);
y = cubeParams(3);
z = cubeParams(4);

% Calculate the location of the seeds
% Seeds must be within the prostate but no too close to it's membrane
% And also not in the uretra and not too close to it
[CX, CY, CZ] = fccStruct(step, x, y, z);
safetyBound = 0.5;
[seedsX, seedsY, seedsZ] = insideSphere(CX, CY, CZ, safetyBound);

seedCount = [seedsX,seedsY,seedsZ];
nOfSeeds = nnz(any(seedCount,2));

% Dose measurement default parameters
cubeSize = 30; % Measurement is always done in a 30x30x30 cube
constVal = 7;
safetyBound = 0.0;

% Measure dose values in a new mesh
% Values measured in the whole sphere without the cylinder
[MX, MY, MZ] = createCube(cubeSize, 0, 0, 0);
[MX, MY, MZ] = insideSphere(MX, MY, MZ, safetyBound);
doseSphere = doseMeasurement(MX, MY, MZ, seedsX, seedsY, seedsZ, constVal);

% Measure the same dose values for the cylinder
[MIX, MIY, MIZ] = createCube(cubeSize, 0, 0, 0);
[MIX, MIY, MIZ] = insideCylinder(MIX, MIY, MIZ, safetyBound);
doseCylinder = doseMeasurement(MIX, MIY, MIZ, seedsX, seedsY, seedsZ, constVal);

% Constraints
doseSphereCols = doseSphere(:);
doseCylinderCols = doseCylinder(:);
V100 = sum(((doseSphereCols ./ 145) .* 100) > 100) / nnz(doseSphereCols) *100;
D90 = (maxk(doseSphereCols, 0.9) / 145) * 100;
V150 = sum(((doseSphereCols ./ 145) .* 100) > 150) / nnz(doseSphereCols) *100;
Du10 = (maxk(doseCylinderCols, 0.1) / 145) * 100;
Du30 = (maxk(doseCylinderCols, 0.3) / 145) * 100;

% Difference from given constraints
V100d = 0.0;
D90d = 0.0;
V150d = 0.0;
Du10d = 0.0;
Du30d = 0.0;
nOfSeedsd = 0.0;
if (V100 < 95)
    V100d = 95 - V100; end
if (D90 < 110)
    D90d = (110 - D90); end
if (V150 > 60)
    V150d = V150 - 60; end
if (Du10 > 150)
    Du10d = (Du10 - 150); end
if (Du30 > 130)
    Du30d = (Du30 - 130); end
if (nOfSeeds > 70)
    nOfSeedsd = (nOfSeeds - 70); end

% Use weights to force the objective function to find the minimum
V100Weight = 3;
D90Weight = 1;
V150Weight = 1;
Du10Weight = 1;
Du30Weight = 1;
seedWeight = 1;

objFuncVal = V100d * V100Weight +...
    D90d * D90Weight + ...
    V150d * V150Weight + ...
    Du10d * Du10Weight + ...
    Du30d * Du30Weight + ...
    nOfSeedsd * seedWeight;
end


% Create the FCC stucture to locate seed points
% Step length and shitf values come from the parameters
function [X, Y, Z] = fccStruct(step, x, y, z)
    % Unit cube with the required base points
    fccbase = [dec2bin(0:7) - ...
        '0'; 0.5 0.5 0; 0.5 0.5 1; 0.5 0 0.5; 0.5 1 0.5; 0 0.5 0.5; 1 0.5 0.5];
    fccbase = fccbase * step;
    
    % Create the grid with the reqiured step length
    [GX, GY, GZ] = ndgrid(0:step:15, 0:step:15, 0:step:15);
    fccXYZ = [GX(:), GY(:), GZ(:)];
    fccXYZ = repmat([GX(:), GY(:), GZ(:)], 14, 1);
    
    % Shift small cube, so the whole grid is covered
    fccXYZ = fccXYZ + ...
        reshape(permute(repmat(fccbase, [1, 1, prod(size(GX))]), [3 1 2]), [], 3);
    fccXYZ = unique(fccXYZ, 'rows');
    
    % Shitf to our dimensions    
    fccXYZ(:,1) = fccXYZ(:, 1) - 3 + x;
    fccXYZ(:,2) = fccXYZ(:, 2) - 3 + y;
    fccXYZ(:,3) = fccXYZ(:, 3) - 3 + z;
    
    % Save the result
    X = fccXYZ(:, 1);
    Y = fccXYZ(:, 2);
    Z = fccXYZ(:, 3);
end

% Creates a regular cube with side length of 5 and center in origin
% Values can be bounded if option is set
function [X, Y, Z] = createCube(res, x, y, z)
    x = linspace(-2.2+x,x+2.2,res);
    y = linspace(-2.2+y,y+2.2,res);
    z = linspace(-2.2+z,z+2.2,res);
    
    [X, Y, Z] = meshgrid(x,y,z);
end

% Throw away those points which are outside from the prostate
% or are inside the urethra
% A safetyBound can be set, so seeds cannot be too close to any of the organs
function [X, Y, Z] = insideSphere(X, Y, Z, safetyBound)
    healDist = sqrt(X.^2 + Y.^2 + Z.^2);
    saveDist = sqrt(X.^2 + Y.^2);
    
    sphereRadius = 2.2;
    cylinderRadius = 0.5;
    X(healDist > sphereRadius) = 0;
    Y(healDist > sphereRadius) = 0;
    Z(healDist > sphereRadius) = 0;
    
    X(saveDist < cylinderRadius + safetyBound) = 0;
    Y(saveDist < cylinderRadius + safetyBound) = 0;
    Z(saveDist < cylinderRadius + safetyBound) = 0;
end

% Keep only the points inside the urethra
function [X, Y, Z] = insideCylinder(X, Y, Z, safetyBound)
    saveDist = sqrt(X.^2 + Y.^2);
    
    cylinderRadius = 0.5;
    X(saveDist > cylinderRadius + safetyBound) = 0;
    Y(saveDist > cylinderRadius + safetyBound) = 0;
    Z(saveDist > cylinderRadius + safetyBound) = 0;
end

% Measure for each meshgrid point the dose values coming from all the seeds
% Dose value is inversely proportional to distance
function dose = doseMeasurement(X, Y, Z, seedsX, seedsY, seedsZ, constVal)
    dose = zeros(size(X,1), size(X,2), size(X,3));
    indexes = find(seedsX)';
    for dim = indexes
        Xdist = X(:,:,:) - repmat(seedsX(dim), size(X,1), size(X,2), size(X,3));
        Xdist(Xdist == -seedsX(dim)) = 0;
        Ydist = Y(:,:,:) - repmat(seedsY(dim), size(X,1), size(X,2), size(X,3));
        Ydist(Ydist == -seedsY(dim)) = 0;
        Zdist = Z(:,:,:) - repmat(seedsZ(dim), size(X,1), size(X,2), size(X,3));
        Zdist(Zdist == -seedsZ(dim)) = 0;
        dose = dose + ((1./sqrt(Xdist.^2 + Ydist.^2 + Zdist.^2)).^2).*constVal;
    end
    dose(isinf(dose)) = 0;
end

% Self implementation of the Matlab built-in maxk function
function value = maxk(cols, part)
    sorted = sort(cols, 'descend');  % Sort the values in descending order
    % Don't deal with zero values because they are not relevant
    index = ceil(part*nnz(sorted));
    if index == 0
        index = 1; end
    value = sorted(index);
end