function multiStart
    LB = [0.7; 0; 0; 0];
    UB = [2; 2; 2; 2];
    OPTS.N100 = 100;
    OPTS.NG0 = 2;
    OPTS.NSIG = 6;
    OPTS.MAXNLM = 2;
    OPTS.MAXFN = 100;
    OPTS.METHOD = 'unirandi';
    FUN =@fccVD2;

    fileID = fopen('.\results\multiRunUpdated.txt', 'a');
    for i=1:10
        tic;
        [x0,f0,NC,NFE] = GLOBAL(FUN, LB, UB, OPTS);
        timeVal = toc;
        fprintf(fileID, '%f %f %f %f\n%f %f %f %f\n%f %f\n%f\n', x0, f0, timeVal);
        fprintf(fileID, '\n');
        disp(i);
    end
    fclose(fileID);
end