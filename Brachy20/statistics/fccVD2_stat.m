% Implementation of FCC (Face centered cubic packing) method:
% The method creates a mesh and puts the radioactive capsules
% alongside it's grid points
% By opimizing it's parameters the method calculates how well the
% constraints are satisfied and minimizes the overall difference
% @params: cubeParams is an array of 4 argument:
% resolution of the cube; x, y, z shifting
% @return: overall difference from the given constraints
function [V100d, D90d, Du10d, Du30d, Dr2cm3d, nOfSeeds] = fccVD2_stat(cubeParams)

% Get the parameters from the input
step = cubeParams(1);
x = cubeParams(2);
y = cubeParams(3);
z = cubeParams(4);

% radialDose_points = [0.1; 0.15; 0.25; 0.5; 0.75; 1; 1.5; 2; 3; 4; 5; 6; 7; 8; 9; 10];
% radialDose_vals = [1.055; 1.078; 1.082; 1.071; 1.042; 1; 0.908; 0.814; 0.632; 0.496; 0.364; 0.270; 0.199; 0.148; 0.109; 0.0803];
% 
% aniFunction_pointsX = [0.5 1 2 3 4 5 6 7 8 9];
% aniFunction_pointsY = [0 5 10 20 30 40 50 60 70 80];
% 
% aniFunction_table = ...
%     [0.3330 0.3700 0.4420 0.4880 0.5200 0.5500 0.5872 0.6407 0.7198 0.8337;
% 0.4000 0.4290 0.4970 0.5350 0.5610 0.5870 0.6185 0.6609 0.7196 0.8003;
% 0.5190 0.5370 0.5800 0.6090 0.6300 0.6450 0.6525 0.6511 0.6394 0.6157;
% 0.7160 0.7050 0.7270 0.7430 0.7520 0.7600 0.7695 0.7829 0.8028 0.8316;
% 0.8460 0.8340 0.8420 0.8460 0.8480 0.8520 0.8585 0.8678 0.8805 0.8970;
% 0.9260 0.9250 0.9260 0.9260 0.9280 0.9280 0.9195 0.8960 0.8509 0.7779;
% 0.9720 0.9720 0.9700 0.9690 0.9690 0.9690 0.9680 0.9652 0.9594 0.9498;
% 0.9910 0.9910 0.9870 0.9870 0.9870 0.9870 0.9891 0.9956 1.0084 1.0298;
% 0.9960 0.9960 0.9960 0.9950 0.9950 0.9950 0.9930 0.9869 0.9747 0.9544;
% 1.0000 1.0000 1.0000 0.9990 0.9990 0.9990 0.9970 0.9909 0.9787 0.9584];
% 
% gF = griddedInterpolant(radialDose_points,radialDose_vals,'nearest','nearest');
% aF = griddedInterpolant({aniFunction_pointsX,aniFunction_pointsY'},aniFunction_table,'nearest','nearest');

% Calculate the location of the seeds
% Seeds must be within the prostate but no too close to it's membrane
% And also not in the uretra and not too close to it
[CX, CY, CZ] = fccStruct(step, x, y, z);
safetyBound = 0.5;
[seedsX, seedsY, seedsZ] = insideSphere(CX, CY, CZ, safetyBound);

seedCount = [seedsX,seedsY,seedsZ];
nOfSeeds = nnz(any(seedCount,2));

% Dose measurement default parameters
cubeSize = 30; % Measurement is always done in a 30x30x30 cube
constVal = 7;
safetyBound = 0.0;

% Measure dose values in a new mesh
% Values measured in the whole sphere without the cylinder
[MX, MY, MZ] = createCube(cubeSize, 0, 0, 0);
[MX, MY, MZ] = insideSphere(MX, MY, MZ, safetyBound);
doseSphere = doseMeasurement(MX, MY, MZ, seedsX, seedsY, seedsZ, constVal);
% doseSphere = anisotropyDoseMeasurement(MX, MY, MZ, seedsX, seedsY, seedsZ, gF, aF);

plot3(MX(:), MY(:), MZ(:), '.r');
hold on;

% Measure the same dose values for the cylinder
[MIX, MIY, MIZ] = createCube(cubeSize, 0, 0, 0);
[MIX, MIY, MIZ] = insideCylinder(MIX, MIY, MIZ, safetyBound);
doseCylinder = doseMeasurement(MIX, MIY, MIZ, seedsX, seedsY, seedsZ, constVal);
% doseCylinder = anisotropyDoseMeasurement(MIX, MIY, MIZ, seedsX, seedsY, seedsZ, gF, aF);

plot3(MIX(:), MIY(:), MIZ(:), '.g');
hold on;

% Measure the same dose values for the cylinder
[MRX, MRY, MRZ] = createCube(cubeSize, 0, 0, 0);
[MRX, MRY, MRZ] = insideHalfCylinder(MRX, MRY, MRZ, safetyBound);
doseHalfCylinder = doseMeasurement(MRX, MRY, MRZ, seedsX, seedsY, seedsZ, constVal);
% doseHalfCylinder = anisotropyDoseMeasurement(MIX, MIY, MIZ, seedsX, seedsY, seedsZ, gF, aF);

plot3(MRX(:), MRY(:), MRZ(:), '.b');
hold off;

% figure('Name','Measurement points','NumberTitle','off');
% plot3(MX(:), MY(:), MZ(:), '.r');
% hold on;
% plot3(MIX(:), MIY(:), MIZ(:), '.g');
% hold on;
% plot3(MRX(:), MRY(:), MRZ(:), '.b');
% hold off;

% Constraint prep
rectumVol = ((acosd(0.5/2)*2)*2*pi/180)*4.4*0.15;
rectumGrids = 2/rectumVol;

% Constraints
doseSphereCols = doseSphere(:); % ezek a prosztat�ban l�v? d�zis�rt�kek, vektork�nt kifesz�tve
% ez l�nyeg�ben kiadja a prosztata t�rfogat�t
doseCylinderCols = doseCylinder(:); % ezek az urethr�ban l�v? d�zis�rt�kek, vektork�nt kifesz�tve
doseHalfCylinderCols = doseHalfCylinder(:);
V100 = sum(((doseSphereCols ./ 145) .* 100) >= 100) / nnz(doseSphereCols) *100; % V100>=95
D90 = (maxk(doseSphereCols, 0.9) / 145 ) * 100; % D90>=110
% V150 = sum(doseSphereCols > 150) / nnz(doseSphereCols) *100; % V150<=60
Du10 = (maxk(doseCylinderCols, 0.1) / 145) * 100; % Du10<=150
Du30 = (maxk(doseCylinderCols, 0.3) / 145) * 100; % Du30<=130
Dr2cm3 = (maxk(doseHalfCylinderCols, rectumGrids) / 145) * 100; % Dr2cm3<=100


% Difference from given constraints
V100d = 0.0;
D90d = 0.0;
Du10d = 0.0;
Du30d = 0.0;
Dr2cm3d = 0.0;
nOfSeedsd = 0.0;

if (V100 < 95)
    V100d = 95 - V100;
end
if (D90 < 110)
    D90d = (110 - D90);
end
if (Du10 > 150)
    Du10d = (Du10 - 150);
end
if (Du30 > 130)
    Du30d = (Du30 - 130);
end
if (Dr2cm3d > 100)
    Dr2cm3d = (Dr2cm3 - 100);
end
if (nOfSeeds > 70)
    nOfSeedsd = (nOfSeeds - 70);
end

end


% Create the FCC stucture to locate seed points
% Step length and shitf values come from the parameters
function [X, Y, Z] = fccStruct(step, x, y, z)
    % Unit cube with the required base points
    fccbase = [dec2bin(0:7) - ...
        '0'; 0.5 0.5 0; 0.5 0.5 1; 0.5 0 0.5; 0.5 1 0.5; 0 0.5 0.5; 1 0.5 0.5];
    fccbase = fccbase * step;
    
    % Create the grid with the reqiured step length
    [GX, GY, GZ] = ndgrid(0:step:15, 0:step:15, 0:step:15);
    fccXYZ = [GX(:), GY(:), GZ(:)];
    fccXYZ = repmat([GX(:), GY(:), GZ(:)], 14, 1);
    
    % Shift small cube, so the whole grid is covered
    fccXYZ = fccXYZ + ...
        reshape(permute(repmat(fccbase, [1, 1, prod(size(GX))]), [3 1 2]), [], 3);
    fccXYZ = unique(fccXYZ, 'rows');
    
    % Shitf to our dimensions        
    fccXYZ(:,1) = fccXYZ(:, 1) - 3 + x;
    fccXYZ(:,2) = fccXYZ(:, 2) - 3 + y;
    fccXYZ(:,3) = fccXYZ(:, 3) - 3 + z;
    
    % Save the result
    X = fccXYZ(:, 1);
    Y = fccXYZ(:, 2);
    Z = fccXYZ(:, 3);
end

% Creates a regular cube with side length of 5 and center in origin
% Values can be bounded if option is set
function [X, Y, Z] = createCube(res, x, y, z)
    x = linspace(-2.2+x,x+2.2,res);
    y = linspace(-3.67+y,y+2.2,res+10);
    z = linspace(-2.2+z,z+2.2,res);

    [X, Y, Z] = meshgrid(x,y,z);
end

% Throw away those points which are outside from the prostate
% or are inside the urethra
% A safetyBound can be set, so seeds cannot be too close to any of the organs
function [X, Y, Z] = insideSphere(X, Y, Z, safetyBound)
    healDist = sqrt(X.^2 + Y.^2 + Z.^2);
    saveDist = sqrt(X.^2 + Y.^2);
    
    sphereRadius = 2.2;
    cylinderRadius = 0.5;
    X(healDist > sphereRadius) = 0;
    Y(healDist > sphereRadius) = 0;
    Z(healDist > sphereRadius) = 0;
    
    X(saveDist < cylinderRadius + safetyBound) = 0;
    Y(saveDist < cylinderRadius + safetyBound) = 0;
    Z(saveDist < cylinderRadius + safetyBound) = 0;
end

% Keep only the points inside the urethra
function [X, Y, Z] = insideCylinder(X, Y, Z, safetyBound)
    saveDist = sqrt(X.^2 + Y.^2);
    
    cylinderRadius = 0.5;
    X(saveDist > cylinderRadius + safetyBound) = 0;
    Y(saveDist > cylinderRadius + safetyBound) = 0;
    Z(saveDist > cylinderRadius + safetyBound) = 0;
end

% Keep only the points inside the rectum
function [X, Y, Z] = insideHalfCylinder(X, Y, Z, safetyBound)
    saveDist = sqrt(X.^2 + (Y+4.32).^2);
    
    outerRadius = 2;
    innerRadius = 1.85;    
    X(saveDist > outerRadius | saveDist < innerRadius) = 0;
    Y(saveDist > outerRadius | saveDist < innerRadius) = 0;
    Z(saveDist > outerRadius | saveDist < innerRadius) = 0;
end

% Measure for each meshgrid point the dose values coming from all the seeds
% Dose value is inversely proportional to distance
function dose = doseMeasurement(X, Y, Z, seedsX, seedsY, seedsZ, constVal)
    dose = zeros(size(X,1), size(X,2), size(X,3));
    indexes = find(seedsX)';
    for dim = indexes
        Xdist = X(:,:,:) - repmat(seedsX(dim), size(X,1), size(X,2), size(X,3));
        Xdist(Xdist == -seedsX(dim)) = 0;
        Ydist = Y(:,:,:) - repmat(seedsY(dim), size(X,1), size(X,2), size(X,3));
        Ydist(Ydist == -seedsY(dim)) = 0;
        Zdist = Z(:,:,:) - repmat(seedsZ(dim), size(X,1), size(X,2), size(X,3));
        Zdist(Zdist == -seedsZ(dim)) = 0;
        dose = dose + ((1./sqrt(Xdist.^2 + Ydist.^2 + Zdist.^2)).^2).*constVal;
    end
    dose(isinf(dose)) = 0;
end

% Self implementation of the Matlab built-in maxk function
function value = maxk(cols, part)
    sorted = sort(cols, 'descend');  % Sort the values in descending order
    % Don't deal with zero values because they are not relevant
    index = ceil(part*nnz(sorted));
    if index == 0
        index = 1; end
    value = sorted(index);
end