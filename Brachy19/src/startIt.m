function startIt
    LB = [0.7; 0; 0; 0];
    UB = [2; 2; 2; 2];
    OPTS.N100 = 100;
    OPTS.NG0 = 2;
    OPTS.NSIG = 6;
    OPTS.MAXNLM = 2;
    OPTS.MAXFN = 100;
    OPTS.METHOD = 'unirandi';
    FUN =@fccVD2;
    tStart = tic;
    [x0,f0,NC,NFE] = GLOBAL(FUN, LB, UB, OPTS);
    tEnd = toc(tStart);
    fprintf('-------------- Relevant info: ----------------------\n');
    fccVD2_disp(x0(:,1));
    fprintf('Optimizer parameters:\n');
    fprintf('OPTS.N100: %d, OPTS.NG0: %d, OPTS.NSIG: %d, OPTS.MAXNLM: %d, OPTS.METHOD: %s\n',...
        OPTS.N100, OPTS.NG0, OPTS.NSIG, OPTS.MAXNLM, OPTS.METHOD);
    fprintf('Used model: %s\n', func2str(FUN));
    fprintf('%d minutes and %f seconds CPU time\n', floor(tEnd/60), rem(tEnd,60));
    fprintf('Global minimizer points:\n');
    disp(x0(:,1));
end