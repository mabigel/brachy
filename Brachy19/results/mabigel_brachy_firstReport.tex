\documentclass[a4paper]{article}

\usepackage[magyar]{babel}
\usepackage[utf8]{inputenc}
\usepackage{color}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{hyperref}

\title{Optimalizálás a műtéti tervezésben \\- további eredmények - első lépések}
\author{\large Mester Abigél és Csendes Tibor\\
\small Szegedi Tudományegyetem}
\date{}

\begin{document}

\maketitle

A brachyterápia daganatos megbetegedések olyan "kíméletes" gyógyítását teszi lehetővé, mellyel az egészséges szervek könnyen megkímélhetők. A külső, roncsoló besugárzás helyett csövek segítségével közvetlen a gyógyítandó szerv belsejébe juttatnak kis intenzitású, sugárzó anyagot. Az ún. seedek ezt követően örökre a szervezetben maradnak, azonban megfelelő elhelyezésükkel elérhető, hogy a kezelés szinte csak a daganatos szervre legyen kihatással. Az optimalizálás eszközeit bevetve célunk, hogy minél rövidebb idő alatt találjuk meg a seedek legjobb elhelyezési struktúráját a szervben. Ezzel mind a beteg várakozási idejét, mind a beavatkozás felmerülő költségeit csökkentve.

\section{A modell}
Kezdetben egy erősen stilizált modellt vettünk, hogy megismerjük a feladat kihívásait - állatorvosi lovunk a prosztatarák volt. Maga a prosztata egy gömbszerű, az abban húzódó húgycső egy hosszúkás testként vizualizálható. Így modelljeinkben ezek gömbként, és annak középpontján áthaladó hengerként jelentek meg, rendre 2,5 cm, valamint 0,5 cm sugárral.

\subsection{Lapközéppontos pakolás}
Ebben a leegyszerűsített térben a feladatunk az volt, hogy a seedeket bizonyos feltételeknek eleget téve helyezzük el. A prosztatába, mint gyógyítandó szerv, helyezhetünk seedeket, viszont az uretrát minél jobban védeni kell. Tehát a cél minél nagyobb denzitás elérése a prosztatában. A geometriában erre használt modell a lapközéppontos pakolás (Face-centered cubic packing). Gauss bizonyította be, hogy a struktúra a legjobb átlagos fedést adja meg a térben.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{fcc_struct.png}
	\caption{A lapközéppontos pakolás által biztosított fedés}
\end{figure}

A seedeket egy ilyen struktúra által meghatározott pontokban helyeztük el. 4 paraméter határozta meg a modell végleges kinézetét, melyek a csúcspontok közötti távolság, valamint a 3 dimenzió mentén való eltolás mértéke. A seed pontok közül továbbá eltávolítottuk azokat melyek az uretrában, vagy ahhoz túl közel helyezkedtek el.

A fedettség ellenőrzéséhez egy további struktúrát hoztunk létre, egy egyszerű 30x30x30-as kockarácsot. Ennek pontjaiban vizsgáltuk a dózisértékeket, és verifikáltuk a továbbiakban ismertetésre kerülő eljárást.

\begin{figure}[h!]	
	\centering
	\includegraphics[width=0.75\textwidth]{model.png}
	\captionsetup{justification=centering}
	\caption{Az elkészült modell illusztrációja:\\ A piros pontok jelölik a gyógyítandó területet, a zöld pontok pedig a megvédendőt - a kockarács pontjai. \\A kék pontok a lapközéppontos struktúra mentén elhelyezett seed pontok.}
	\label{fig2}
\end{figure}

\section{Implementáció}
A modellt és számításokat Matlabban készítettük el. Ennek egyszerű oka, hogy a számításokhoz használt optimalizáló eljárás - a Számítógépes Optimalizálás Tanszék által fejlesztett Global - egy változata Matlabban készült. Valamint a Matlab kiválóan alkalmas geometriai struktúrák kezelésére.

Az általunk készített eljárás első lépésként megalkotja a \ref{fig2} ábrán látható modellt. Majd a kockarács minden pontjába kiszámítja a dózisértékeket. Ez egy kumulatív összeg, mely az alábbi képlet alapján adódik:
$$ dose_i = constantValue * \sum_{j=1}^{numOfSeeds}\frac{1}{dist(point_i, seed_j)^2}$$

A konstans érték adja a pontos dózis értékeket.

A rácspontokban kapott dózisértékek alapján már számolható az orvosok által gyakran emlegetett 5 zöld lámpa, mely lényegében a célfüggvényünket adja meg:
$$ V_{100} \geq 95\% $$
$$ D_{90} \geq 110\% $$
$$ V_{150} \leq 60\% $$
$$ D_{U90} \leq 150\% $$
$$ D_{U70} \leq 130\% $$

Az első 3 feltétel a prosztatára vonatkozik, míg utóbbi kettő a védett uretrára.

Egy-egy V-s érték azt jelenti, hogy a céltérfogat hány százaléka kapja meg az előírt dózis alsó indexben jelölt százalékát. Így például az első feltételnél 100\% a cél, mely pontos értékben 145 Gy-t besugárzást jelent, legalább a céltérfogat 95\%-án.

Egy-egy D-s érték jelentése pedig, hogy a céldózis hány százalékát kapja meg a céltérfogat szintén alsó indexben jelölt százaléka. Tehát a második feltétel a gömb területének 90\%-ban 110\%-os (Grayben ~160-as) értéket vár.

A pontokban mért dózisértékekből minden feltételre kiszámoltuk, hogy annak mennyiben teszünk eleget, és a célfüggvényünk az ettől vett eltérést minimalizálja:
$$ objFunc = V_{100}d + D_{90}d + V_{150}d + D_{U90}d + D_{U70}d$$

A célfüggvényben további súlyok használhatók, mivel az egyes feltételek kielégítése nem egyforma fontossággal bír.

\section{Optimalizálás}
Az elkészült eljárást a Global optimalizálónak adtuk oda. Az optimalizáló képes több lokális minimummal rendelkező függvény globális minimumát megtalálni. Az alábbiak szerint paraméterezhető:
\begin{verbatim}
	LB = [0.7; 0; 0; 0];
	UB = [2; 2; 2; 2];
	OPTS.N100 = 1000;
	OPTS.NG0 = 2;
	OPTS.NSIG = 12;
	OPTS.MAXNLM = 2;
	OPTS.METHOD = 'unirandi';
	FUN =@fccVD2;
\end{verbatim}

Az LB és UB változók adják az alsó és felső korlátot az állítandó 4 paraméterre, a kocka csúcspontjainak távolságára, és a 3 dimenzió menti eltolásra. A többi már az optimalizálónak szól, hogy milyen pontosságig, mennyi ideig dolgozzon, valamint milyen optimalizáló módszert alkalmazzon.

Az optimalizálót az alábbiakkal indíthatjuk el:
\begin{verbatim}
	[x0,f0,NC,NFE] = GLOBAL(FUN, LB, UB, OPTS);
\end{verbatim}

\section{Eredmények}
A stilizált modellek segítségével viszonylag gyors futási mellett sikerült elérnünk az "5 zöld lámpát".

\begin{verbatim}
	Model parameters:
	Safety bound: 0.600
	Constraints:
	- V100: 95.02% >= 95% ++ (error: 0.00%)
	-  D90: 209.43% >= 110% ++ (error: 0.00%)
	- V150: 57.48% <= 60% ++ (error: 0.00%)
	- Du90: 133.52% <= 150% ++ (error: 0.00%)
	- Du70: 127.22% <= 130% ++ (error: 0.00%)
	Number of seeds: 100
	Total difference (obj. func.): 0.00%
	Used weigth in objective function: 
	- V100: 1.00
	-  D90: 1.00
	- V150: 1.00
	- Du90: 1.00
	- Du70: 1.00
	Optimizer parameters:
	OPTS.N100: 1000, OPTS.NG0: 2, OPTS.NSIG: 12, OPTS.MAXNLM: 2, OPTS.METHOD: unirandi
	Used model: fccVD2
	2 minutes and 49.099814 seconds CPU time
	Global minimizer points:
	1.2507
	0.5138
	0.4552
	1.3176
\end{verbatim}

Az hisztogramok mutatják, hogy a dózisértékek jelentős százaléka a megengedett határokon belül mozog.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.85\textwidth]{prostate_hist.png}
	\captionsetup{justification=centering}
	\caption{A prosztatában kapott dózisértékek eloszlása logaritmikus skálára levetítve.}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.85\textwidth]{uretra_hist.png}
	\captionsetup{justification=centering}
	\caption{Az uretrában kapott dózisértékek eloszlása.}
\end{figure}

\section{További tervek}
A kezdeti modellünket verifikáltuk, így a továbbiakban lépésről lépésre közelítünk a szervek valós kinézete felé, valamint a seedek elhelyezése kapcsán felmerülő valós problémák felé. Mint például, hogy a seedeket egy bizonyos rács mentén átjuttatott tűkön keresztül lehet csak bejuttatni a szervbe, mely újabb kihívások elé állít majd minket.

\end{document}
