fid = fopen('../input_data/beteg1_101010.xyz');
X = [];
Y = [];
Z = [];
O = [];

tline = fgetl(fid);
while ischar(tline)
    newIn = str2num(tline);
    if (newIn(4) ~= 10 & newIn(5) ~= 10 & newIn(6) ~= 10)
        X = [X; newIn(1)];
        Y = [Y; newIn(2)];
        Z = [Z; newIn(3)];
        O = [O; newIn(7)];
    end
    tline = fgetl(fid);
end
fclose(fid);

prostateX = X(O == 0);
prostateY = Y(O == 0);
prostateZ = Z(O == 0);
urethraX = X(O == 2);
urethraY = Y(O == 2);
urethraZ = Z(O == 2);
rectumX = X(O == 4);
rectumY = Y(O == 4);
rectumZ = Z(O == 4);